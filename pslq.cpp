#include <boost/math/constants/constants.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/math/special_functions/zeta.hpp>
#include <vector>
#include <iostream>

using namespace boost::numeric::ublas;

typedef long long lint;


/* This function inverts a nxn lower triangular matrix
 *
 *    T = 1 + N
 *
 * One can show that N^n = 0. Hence,
 *
 *    1 = 1-(-N)^n = (1-(-N))(1+(-N)+(-N)^2+...+(-N)^(n-1))
 *
 * In other words,
 *
 *    T^-1 = (1+(-N)+(-N)^2+...+(-N)^(n-1))
 *
 *         = 1 + Sum[(-1)^k N^k, {k,1,n-1}]
 */
template<class T>
matrix<T> InvertMatrix(const matrix<T>& input) {
    int n = input.size1();
    matrix<T> I(n,n), N(n,n);
    I.assign(identity_matrix<T>(n));
    N = input - I;
    matrix<T> Nk(N);

    for(int k=1; k<= n-1; k++)
    {
        I = I + (k%2 ? -1 : +1) * Nk;
        Nk = prod(Nk, N);
    }

    return I;
}

template <typename T>
matrix<lint> hermitereduce(matrix<T> &H)
{
    int n = H.size1();
    matrix<lint> D(n,n);
    D.assign(identity_matrix<lint>(n));

    matrix<T> Hold(H);

    for(int i=1; i<n; i++)
    {
        for(int j=i-1; j >= 0; j--)
        {
            T qq = Hold(i,j) / Hold(j,j);
            /* Round q to the nearest integer */
            qq = qq + typename T::number(qq.sign()) / 2;
            int q = qq.template convert_to<int>();

            for(int k=0; k <= j; k++)
                H(i,k) -= q * H(j,k);
            for(int k=0; k < n; k++)
                D(i,k) -= q * D(j,k);
        }
    }
    return D;
}

template <typename T>
bool pslq(std::vector<T> basis, std::vector<lint> &rel, T zero, int limit)
{
    int n = basis.size();
    /* PSLQ needs normalised inputs */
    vector<T> x(n);
    T norm = 0;
    for(int i=0; i < n; i++)
    {
        norm += basis.at(i) * basis.at(i);
        x(i) = basis.at(i);
    }

    x = x / sqrt(norm);

    /* Build s_i^2 = \sum_{j=i}^n x_j^2 */
    vector<T> s(n);
    for(int i=0; i < n; i++)
    {
        for(int j=i; j < n; j++)
            s(i) = s(i) + x(j)*x(j);
        s(i) = sqrt(s(i));
    }

    /* Build the H_x matrix from (1) */
    matrix<T> H(n, n-1);
    for(int i=0; i < n; i++)
    {
        for(int j=0; j < n-1; j++)
        {
            if(i>j)
                H(i,j) = -x(i)*x(j)/s(j)/s(j+1);
            else if(i==j)
                H(i,j) = s(i+1)/s(i);
            else
                H(i,j) = 0;
        }
    }

    matrix<lint> A(n,n), B(n,n);
    A.assign(identity_matrix<lint>(n));
    B.assign(identity_matrix<lint>(n));

    matrix<lint> D = hermitereduce(H);
    matrix<lint> Dinv(n,n);
    Dinv = InvertMatrix(D);
    /* A accumulates the D matrices, B the inverse */
    A = prod(D, A);
    B = prod(Dinv, B);
    x = prod(x, Dinv);

    for(int iter=0; iter <= limit; iter++)
    {
        T beta, lambda, delta;

        /* Step One */

        /* Let r be such that \gamma^j |(AH_x Q)_jj | is maximal for j = r. */
        int r;
        T gammaj = 1;
        T acc = 0;
        for(int j=0; j < n-1; j++)
        {
            gammaj *= 2/sqrt(3);
            if(acc < gammaj * abs(H(j,j)))
            {
                acc = gammaj * abs(H(j,j));
                r = j;
            }
        }
        /* We will need these for Step 2 */
        if(r < n-2)
        {
            beta = H(r+1,r);
            lambda = H(r+1,r+1);
            delta = sqrt(beta*beta + lambda*lambda);
        }

        /* R is the identity matrix with row r and r+1 swapped */
        matrix<lint> R(n,n);
        for(int i=0; i<n; i++)
            for(int j=0; j<n; j++)
                R(i,j) = 0;
        for(int i=0; i<n; i++)
        {
            if(i==r)
                R(i,i+1) = 1;
            else if(i==r+1)
                R(i,i-1) = 1;
            else
                R(i,i) = 1;
        }

        /* Update */
        x = prod(x, R);
        H = prod(R, H);
        A = prod(R, A);
        B = prod(B, R);

        /* Step Two */
        if(r < n-2)
        {
            matrix<T> P(n-1,n-1);
            for(int i=0; i < n-1; i++)
            {
                for(int j=0; j < n-1; j++)
                {
                    if( (i==r)&&(j==r) )
                        P(i, j) = beta/delta;
                    else if( (i==r)&&(j==r+1) )
                        P(i, j) = -lambda/delta;
                    else if( (i==r+1)&&(j==r) )
                        P(i, j) = +lambda/delta;
                    else if( (i==r+1)&&(j==r+1) )
                        P(i, j) = beta/delta;
                    else if((i==j)&&((j!=r)||(j!=r+1)))
                        P(i, j) = 1;
                }
            }
            H = prod(H, P);
        }

        /* If there's a zero on the diagonal, we failed */
        for(int i=0; i < n-1; i++)
            if(abs(H(i,i)) < zero)
                return false;

        /* Step Three: hermite reduce and update */

        D = hermitereduce(H);
        Dinv = InvertMatrix(D);
        /* A accumulates the D matrices, B the inverse */
        A = prod(D, A);
        B = prod(B, Dinv);
        x = prod(x, Dinv);

        /* Step Four */
        /* If any of the elements of x or the diagonal of H are below zero, we're done */
        for(int i=0; i < n; i++)
            if(abs(x(i)) < zero)
                goto done;
        for(int i=0; i < n-1; i++)
            if(abs(H(i,i)) < zero)
                goto done;
    }
    return false;

done:

    /* We now need to find the smallest entry in |x| */
    int ind;
    T tmp=1;
    for(int i=0; i < n; i++)
    {
        if(abs(x(i)) < tmp)
        {
            tmp = abs(x(i));
            ind = i;
        }
    }

    for(int i=0; i < n; i++)
        rel.push_back(column(B, ind)(i));

    return true;
}


typedef boost::multiprecision::cpp_dec_float_50 value_type;
int main(){
    value_type two = 2;

    std::vector<value_type> basis;

    /* Set the basis to be Zeta/@{2,3,4} */
    basis.push_back(boost::math::zeta(two));
    basis.push_back(boost::math::zeta(two+1));
    basis.push_back(boost::math::zeta(two+2));

    /* Have some strange number */
    basis.insert(basis.begin(), 449*basis.at(0)/347 + 311 * basis.at(1) / 307 + 479 * basis.at(2) / 467);

    std::cout << std::setprecision(100) << basis.at(3) << std::endl;

    value_type eps = 10;
    eps = pow(10, -45);

    std::vector<lint> rel;
    if(pslq(basis, rel, eps, 100))
        for(auto i=rel.cbegin(); i != rel.cend(); i++)
            std::cout << *i << std::endl;
}
