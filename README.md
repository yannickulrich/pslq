# PSLQ

A C++ implementation of the PSLQ algorithm.
It uses boost for high precision arithmetic and matrix operations.
Based on the [Mathematica implementation](https://library.wolfram.com/infocenter/MathSource/4263/) by Peter Bertok.

Further reading: [A gentle introduction to PSLQ](https://arminstraub.com/downloads/math/pslq.pdf) by Armin Straub.
